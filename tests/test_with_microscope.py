"""
Test code for the microscope client.

Bear in mind that this requires a microscope to be running on the
local network, and findable via DNS-SD.  We may in time be able
to use the openflexure microscope dummy server for this.

Copyright Richard Bowman 2020, released under GNU GPL v3
"""
import pytest
import logging
import openflexure_microscope_client as ofm_client

@pytest.fixture(scope="session")
def microscope(request):
    """Return a connection to a microscope
    
    We pick the microscope as per the ``--server`` command line option.

    Return value:
        microscope - a microscope client object
    """
    server = request.config.getoption("--server")
    attempts = []
    try:
        if server is None:
            attempts.append("localhost")
            logging.debug("Attempting to connect to localhost")
            try:
                return ofm_client.MicroscopeClient("localhost")
            except Exception as e:
                logging.info(f"No microscope server running on localhost, trying mDNS")
                logging.debug(f"connection error: {e}")

        if server is None or server == "mDNS":
            logging.debug("Attempting to find a microscope using mDNS")
            attempts.append("mDNS")
            return ofm_client.find_first_microscope()
            
        logging.debug(f"Attempting to connect to '{server}'")
        attempts.append(f"hostname: '{server}'")
        return ofm_client.MicroscopeClient(server)
    except:
        pytest.fail(f"Could not connect to a microscope, tried {', '.join(attempts)}")


def test_position_return_value(microscope):
    """Check that the position is returned as a 3-element dictionary"""
    pos = microscope.position
    assert "x" in pos
    assert "y" in pos
    assert "z" in pos

def test_stage_moves(microscope):
    """Wiggle the stage, and check that it moves"""
    if microscope.configuration["stage"]["type"] == "MissingStage":
        pytest.skip("Can't run this test with a missing stage")
    pos = microscope.position
    starting_pos = pos.copy()
    pos['x'] += 100
    microscope.move(pos)
    assert microscope.position == pos
    pos['x'] -= 100
    microscope.move(pos)
    assert microscope.position == starting_pos

def test_autofocus(microscope):
    """Check the microscope will autofocus"""
    ret = microscope.autofocus()
    #TODO: check return value more carefully, check execution time?

def test_grab_image(microscope):
    """Acquire an image - should raise an exception if we don't get a JPEG"""
    image = microscope.grab_image()
    
def test_mjpeg_stream(microscope, caplog):
    """Connect to the microscope and try to retrieve frames from the mjpeg stream"""
    b = microscope.grab_mjpeg(2)
    frames_found = 0
    for image in ofm_client.iterate_mjpeg_images(b):
        logging.debug("Successfully reconstructed a PIL image object")
        frames_found += 1
    assert frames_found > 0
