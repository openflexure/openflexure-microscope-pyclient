import pytest
import logging
import openflexure_microscope_client as ofm_client

def pytest_addoption(parser):
    parser.addoption(
        "--server", action="store", default=None,
    )